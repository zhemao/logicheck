import csv

def _str_to_bool(s):
    return False if s == '0' else True

class Checker(object):
    def __init__(self, truthtable):
        self.inputs = []
        self.outputs = []
        
        for row in truthtable:
            self.inputs.append(row[0])
            self.outputs.append(row[1])

    def _simple_check(self, i, func, minterm, expected):
        args = tuple([_str_to_bool(c) for c in minterm]) 
        res = func(*args)

        if expected == '0' and res:
            print "Function %d fails check for minterm %s" % (i, minterm)
            print "Expected 0, instead got 1"
            return False

        if expected == '1' and not res:
            print "Function %d fails check for minterm %s" % (i, minterm)
            print "Expected 1, instead got 0"
            return False

        return True

    def check_functions(self, functions):
        passed = True

        for i, func in enumerate(functions):
            for minterm in self.inputs:
                expected = self.outputs[i]
                if not self._simple_check(i, func, minterm, expected):
                    passed = False

        if passed:
            print "All functions are correct"

def check_csv(filename, *functions):
    with open(filename) as f:
        checker = Checker(csv.reader(f))
        checker.check_functions(functions)
